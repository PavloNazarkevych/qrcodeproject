package com.example.pavli.qrcodenew;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RefactorActivity extends AppCompatActivity {

    private String loadImg = "";
    private String imagePath = "";
    private String pictureImagePath = "";
    private static int RESULT_LOAD_IMAGE = 12345;
    Button btnRefactorSave, btnRefactorPhoto;
    EditText editRefactorName, editRefactorPhone, editRefactorEmail, editRefactorDiscount;
    ImageView imgRefactor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refactor);

        btnRefactorSave = (Button) findViewById(R.id.btnRefactorSave);
        btnRefactorPhoto = (Button) findViewById(R.id.btnRefactorPhoto);

        editRefactorName = (EditText) findViewById(R.id.editRefactorName);
        editRefactorPhone = (EditText) findViewById(R.id.editRefactorPhone);
        editRefactorEmail = (EditText) findViewById(R.id.editRefactorEmail);
        editRefactorDiscount = (EditText) findViewById(R.id.editRefactorDiscount);

        imgRefactor = (ImageView) findViewById(R.id.imgRefactor);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String discount = intent.getStringExtra("discount");
        String name = intent.getStringExtra("name");
        String phone = intent.getStringExtra("phone");
        String email = intent.getStringExtra("email");
        loadImg = intent.getStringExtra("image");
        imagePath = intent.getStringExtra("imagePath");

        editRefactorDiscount.setText(discount);
        editRefactorName.setText(name);
        editRefactorPhone.setText(phone);
        editRefactorEmail.setText(email);

        if (!"".equals(loadImg)) {

            File file = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), loadImg);

            Glide
                    .with(this)
                    .load(file)
                    .into(imgRefactor);
        } else {
            Glide
                    .with(this)
                    .load(imagePath)
                    .into(imgRefactor);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onClickRefactorPhoto(View view) {
        openBackCamera();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            final File imgFile = new  File(pictureImagePath);
            if(imgFile.exists()){
                imagePath = "";
                File file = new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), loadImg);

                Glide
                        .with(this)
                        .load(file)
                        .into(imgRefactor);
            }
        } else if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);
            cursor.close();


            imgRefactor.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            loadImg = "";

        }
    }


    private  void openBackCamera() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        loadImg = timeStamp + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        pictureImagePath = storageDir.getAbsolutePath() + "/" + loadImg;

        File file = new File(pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, 1);

    }

    public void onClickRefactorSave(View view) {
        String name = editRefactorName.getText().toString();
        String phone = editRefactorPhone.getText().toString();
        String email = editRefactorEmail.getText().toString();
        String discount = editRefactorDiscount.getText().toString();

        if (!"".equals(name)) {
            Intent returnIntetn = new Intent();

            returnIntetn.putExtra("discount", discount);
            returnIntetn.putExtra("imagePath", imagePath);
            returnIntetn.putExtra("namePhoto", loadImg);
            returnIntetn.putExtra("name", name);
            returnIntetn.putExtra("phone", phone);
            returnIntetn.putExtra("email", email);
            setResult(RefactorActivity.RESULT_OK, returnIntetn);
            finish();
        } else {
            Toast.makeText(this, "Write Name Card", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickRefactorGallery(View view) {

        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);

    }
}

