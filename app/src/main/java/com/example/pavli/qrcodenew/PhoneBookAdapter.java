package com.example.pavli.qrcodenew;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavli on 21.12.2017.
 */

public class PhoneBookAdapter extends BaseAdapter implements Filterable {
    private Context mContext;


    //original array
    private List<PhoneBook> mPhoneBooks;
    //filtered array
    List<PhoneBook> mFilteredPhoneBooks;

    ValueFilter valueFilter;


    public PhoneBookAdapter(Context context, List<PhoneBook> list) {
        mContext = context;
        mPhoneBooks = list;
        mFilteredPhoneBooks = list;
    }


    @Override
    public int getCount() {
        return mPhoneBooks.size();
    }

    @Override
    public Object getItem(int pos) {
        return mPhoneBooks.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // get selected entry
        final PhoneBook entry;

        entry = mPhoneBooks.get(pos);

        ViewHolder holder;


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item, null);
            holder = new ViewHolder();
            holder.discount = (TextView) convertView.findViewById(R.id.tvDiscount);
            holder.name = (TextView) convertView.findViewById(R.id.tvName);
            holder.phone = (TextView) convertView.findViewById(R.id.tvPhone);
            holder.email = (TextView) convertView.findViewById(R.id.tvEmail);
            holder.icon = (ImageView) convertView.findViewById(R.id.imgAvatar);
            holder.btn = (Button) convertView.findViewById(R.id.btnCall);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.btn.setFocusable(true);
        holder.btn.setClickable(true);
        holder.btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + entry.getPhone()));
                mContext.startActivity(intent); }
        });



        if (!"".equals(entry.getsStr())) {


            File file = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), entry.getsStr());

            Glide
                    .with(mContext)
                    .load(file)
                    .into(holder.icon);
        } else {
            Glide
                    .with(mContext)
                    .load(entry.getmPath())
                    .into(holder.icon);

        }

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ActivityCard.class);
                intent.putExtra("image", entry.getsStr());
                intent.putExtra("imagePath", entry.getmPath());
                mContext.startActivity(intent);
            }
        });
        // set name
        holder.name.setText(entry.getName());

        // set phone
        holder.phone.setText(entry.getPhone());

        // set email
        holder.email.setText(entry.getEmail());

        //set discount
        holder.discount.setText("Discount: "+ entry.getmDiscounts() + "%");

        return convertView;
    }


    @Override
    public Filter getFilter() {

        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }



    static class ViewHolder {
        Button btn;
        TextView discount;
        TextView name;
        TextView phone;
        TextView email;
        ImageView icon;
    }

    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

               ArrayList<PhoneBook> filterList = new ArrayList<PhoneBook>();

                for (int i = 0; i < mFilteredPhoneBooks.size(); i++) {

                    if ((mFilteredPhoneBooks.get(i).getName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        PhoneBook phoneBook = new PhoneBook(mFilteredPhoneBooks.get(i).getmDiscounts(),
                                mFilteredPhoneBooks.get(i).getmPath(),
                                mFilteredPhoneBooks.get(i).getsStr(),
                                mFilteredPhoneBooks.get(i).getName(),
                                mFilteredPhoneBooks.get(i).getPhone(),
                                mFilteredPhoneBooks.get(i).getEmail());

                        filterList.add(phoneBook);

                    } else if (mFilteredPhoneBooks.get(i).getmDiscounts().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        PhoneBook phoneBook = new PhoneBook(mFilteredPhoneBooks.get(i).getmDiscounts(),
                                mFilteredPhoneBooks.get(i).getmPath(),
                                mFilteredPhoneBooks.get(i).getsStr(),
                                mFilteredPhoneBooks.get(i).getName(),
                                mFilteredPhoneBooks.get(i).getPhone(),
                                mFilteredPhoneBooks.get(i).getEmail());

                        filterList.add(phoneBook);
                    }
                }

                results.count = filterList.size();
                results.values = filterList;

            } else {
                results.count = mFilteredPhoneBooks.size();
                results.values = mFilteredPhoneBooks;
            }
            return results;

        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            mPhoneBooks = (ArrayList<PhoneBook>) results.values;
            notifyDataSetChanged();
        }
    }

}

