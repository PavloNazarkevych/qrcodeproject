package com.example.pavli.qrcodenew;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.baoyz.actionsheet.ActionSheet;
import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityAddCard extends AppCompatActivity implements ActionSheet.ActionSheetListener {

    EditText editName,editEmail, editPhone, editDiscount;
    Button btnSave;
    ImageView imgCard;
    private String pictureImagePath = "";
    private static int RESULT_LOAD_IMAGE = 1234;
    private String picturePath;

    public String textNameCard;
    public String textNameShop;
    public String textPhone;
    public String textDiscount;
    String loadImg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        if (savedInstanceState != null) {
             loadImg = savedInstanceState.getString("message");
        }

        editName = (EditText) findViewById(R.id.editName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPhone = (EditText) findViewById(R.id.editPhone);
        editDiscount = (EditText) findViewById(R.id.editDiscount);

        btnSave = (Button) findViewById(R.id.btnSave);
        imgCard = (ImageView) findViewById(R.id.imgCard);
        imgCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionSheet.createBuilder(ActivityAddCard.this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Open Camera", "From Gallery")
                        .setCancelableOnTouchOutside(true)
                        .setListener(ActivityAddCard.this).show();
            }
        });



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Glide.with(this)
                .load(R.drawable.card2)
                .into(imgCard);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.getMenuInflater().inflate(R.menu.photo_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Toast.makeText(PopupMenuDemoActivity.this,
                // item.toString(), Toast.LENGTH_LONG).show();
                // return true;
                switch (item.getItemId()) {

                    case R.id.camera:
                        openBackCamera();
                        return true;
                    case R.id.gallery:
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu menu) {

            }
        });
        popupMenu.show();
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("message", loadImg);
        super.onSaveInstanceState(outState);
    }



    public void onClickPhoto(View v) {

        showPopupMenu(v);
        //openBackCamera();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            final File imgFile = new  File(pictureImagePath);
            if(imgFile.exists()){

                picturePath = "";
                File file = new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), loadImg);

                Glide
                        .with(this)
                        .load(file)
                        .into(imgCard);
            }
        } else if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
             picturePath = cursor.getString(columnIndex);
            cursor.close();


            imgCard.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            loadImg = "";

        }
    }

    private  void openBackCamera() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        loadImg = timeStamp + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        pictureImagePath = storageDir.getAbsolutePath() + "/" + loadImg;

        File file = new File(pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, 1);

    }

    //Передаю в основне Activity дані для створенн нового елемента
    public void onClickSave(View v) {

        textNameCard = editName.getText().toString();
        textNameShop = editEmail.getText().toString();
        textPhone = editPhone.getText().toString();
        textDiscount = editDiscount.getText().toString();

        if (!"".equals(textNameCard)) {
            Intent returnIntetn = new Intent();

            returnIntetn.putExtra("namePhoto", loadImg);
            returnIntetn.putExtra("picturePath", picturePath);
            returnIntetn.putExtra("nameDiscount", textDiscount);
            returnIntetn.putExtra("nameCard", textNameCard);
            returnIntetn.putExtra("nameShop", textNameShop);
            returnIntetn.putExtra("phone", textPhone);

            setResult(ActivityAddCard.RESULT_OK, returnIntetn);
            finish();
        } else {
            Toast.makeText(this, "Write Name Card", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        switch (index) {
            case 0:
                openBackCamera();
                break;
            case 1:
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;
            default:
                break;
        }
    }

//    public void onClickGallery(View view) {
//
//        Intent i = new Intent(
//                Intent.ACTION_PICK,
//                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//        startActivityForResult(i, RESULT_LOAD_IMAGE);
//
//    }
}
