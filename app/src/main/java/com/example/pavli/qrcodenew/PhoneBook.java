package com.example.pavli.qrcodenew;

import android.graphics.Bitmap;

/**
 * Created by pavli on 21.12.2017.
 */

public class PhoneBook {
    private Bitmap mAvatar;
    private int bBitmap;
    private byte btArray;
    private String mDiscounts;
    private String mName;
    private String mPath;
    private String sStr;
    private String mPhone;
    private String mEmail;

    public PhoneBook(String discounts,String path, String str, String name, String phone, String email) {
        mDiscounts = discounts;
        mPath = path;
        sStr = str;
        mName = name;
        mPhone = phone;
        mEmail = email;
    }

    public PhoneBook(PhoneBook other) {
        this(other.getmDiscounts(), other.getmPath(), other.getsStr(),other.getName(),other.getPhone(),other.getEmail());
    }


    public void setmDiscounts(String discounts) {
        mDiscounts = discounts;
    }

    public String getmDiscounts() {
        return mDiscounts;
    }

    public void setmPath(String path) {
        mPath = path;
    }

    public String getmPath() {
        return mPath;
    }

    public void setsStr(String str) {
        sStr = str;
    }

    public String getsStr() {
        return sStr;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }
    public String getPhone() {
        return mPhone;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getEmail() {
        return mEmail;
    }

}
