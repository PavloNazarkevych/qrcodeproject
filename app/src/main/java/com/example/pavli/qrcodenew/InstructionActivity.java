package com.example.pavli.qrcodenew;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class InstructionActivity extends AppCompatActivity {

    TextView tvFirst, tvSecond, tvThird, tvForth, tvEnd, tvInstruction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        tvInstruction = (TextView) findViewById(R.id.tvInstruction);
        tvFirst = (TextView) findViewById(R.id.tvFirst);
        tvSecond = (TextView) findViewById(R.id.tvSecond);
        tvThird = (TextView) findViewById(R.id.tvThird);
        tvForth = (TextView) findViewById(R.id.tvForth);
        tvEnd = (TextView) findViewById(R.id.tvEnd);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
