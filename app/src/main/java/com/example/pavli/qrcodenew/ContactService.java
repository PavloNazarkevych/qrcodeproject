package com.example.pavli.qrcodenew;

import java.util.ArrayList;

/**
 * Created by pavli on 21.12.2017.
 */

public class ContactService implements Cloneable{

    private ArrayList<PhoneBook> phoneBooks;

    private static ContactService ourInstance = new ContactService();

    public static ContactService getInstance() {
        return ourInstance;
    }

    void setInstance(ContactService instance) {
        this.ourInstance = instance;
    }

    protected ContactService() {
        this.defaultContacts();
    }

    public void addPhoneBook(PhoneBook book) {
        this.phoneBooks.add(book);
    }

    public void deleteBook(PhoneBook book) {
        this.phoneBooks.remove(book);
    }

    public ArrayList<PhoneBook> getPhoneBooks() {
        return phoneBooks;
    }

    public void setPhoneBooks(ArrayList<PhoneBook> phoneBooks) {
        this.phoneBooks = phoneBooks;
    }

    //створення по дефолту елементів
    public void defaultContacts() {
        this.phoneBooks = new ArrayList<PhoneBook>();
        String[] discounts = {"", "", ""};
        String[] paths = {"", "", ""};
        String[] strs = {"sss", "ssss", "sssss"};
        String[] names = {"Pete Houston", "Lina Cheng", "Jenny Nguyen"};
        String[] phonenumber = { "0934979876", "0507563445", "0506611279"};
        String[] email = { "pete.houston.17187@gmail.com", "lina.cheng011@sunny.com", "jenny_in_love98@yahoo.com"};
        for (int i = 0;i < names.length; i++) {
            PhoneBook phoneBook = new PhoneBook(discounts[i],paths[i],strs[i],names[i],phonenumber[i],email[i]);
            this.phoneBooks.add(phoneBook);
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
