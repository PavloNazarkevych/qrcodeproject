package com.example.pavli.qrcodenew;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvPhone;
    final int MENU_RESET_ID = 1;
    final int MENU_QUIT_ID = 2;
    final int MENU_ABOUT_APP_ID = 3;
    final int MENU_INSTRUCTION_ID = 4;
    private final int requestAddCard = 1000;
    private final int requestRefactorCard = 2000;
    private static final int CM_DELETE_ID = 1001;
    private static final int CM_REFACTOR_ID = 1002;
    private static long back_pressed;
    private int itemPos;
    private String picturePass;



    String discount;
    String nameCard;
    String nameShop;
    String namePhoto = "";
    String phone;
    SearchView searchEt;

    private ArrayList<PhoneBook> phoneBooks;
    PhoneBookAdapter adapter;
    AdapterView.AdapterContextMenuInfo acmi;
    List<PhoneBook> listPhoneBook;
    PhoneBook selectedBook;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lvPhone = (ListView)findViewById(R.id.listPhone);
        registerForContextMenu(lvPhone);

        searchEt = (SearchView) findViewById(R.id.searchEt);

        //Load data
        reloadData();

        phoneBooks = ContactService.getInstance().getPhoneBooks();

        sorting();
        
        //update adapter
        adapter = new PhoneBookAdapter(this, phoneBooks);
        lvPhone.setAdapter(adapter);

        searchEt.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }


        });


        lvPhone.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ArrayList<PhoneBook> books = phoneBooks;
                selectedBook = books.get(position);

                Intent intent = new Intent (MainActivity.this, ActivityCard.class);
                intent.putExtra("image", selectedBook.getsStr());
                intent.putExtra("imagePath", selectedBook.getmPath());
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) super.onBackPressed();
        else Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_RESET_ID, 0, "Reset");
        menu.add(0, MENU_ABOUT_APP_ID, 0, "About app");
        menu.add(0, MENU_INSTRUCTION_ID, 0, "Instruction");
        menu.add(0, MENU_QUIT_ID, 0, "Quit");
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_RESET_ID:
                final Dialog dialog = new Dialog(this);
                dialog.setTitle("Reset data");
                dialog.setContentView(R.layout.dialog_view);
                final AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

                final TextView edRefactor = (TextView) dialog.findViewById(R.id.edRegactor);
                dialog.show();

                Button submit = (Button) dialog.findViewById(R.id.btnSubmit);
                Button cancel = (Button) dialog.findViewById(R.id.btnCancel);

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listPhoneBook = ContactService.getInstance().getPhoneBooks();
                        listPhoneBook.clear();
                        adapter = new PhoneBookAdapter(MainActivity.this, ContactService.getInstance().getPhoneBooks());
                        lvPhone.setAdapter(adapter);
                        dialog.dismiss();
                        saveData();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case MENU_QUIT_ID:
                finish();
                break;
            case MENU_ABOUT_APP_ID:
                Toast.makeText(MainActivity.this, "Nothing", Toast.LENGTH_SHORT).show();
                break;
            case MENU_INSTRUCTION_ID:
                Intent intent = new Intent(MainActivity.this, InstructionActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Add element using new Activity, fill all required fields
    public void onClickAddContact(View view) {
        Intent intent = new Intent(MainActivity.this, ActivityAddCard.class);
        startActivityForResult(intent,requestAddCard);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == requestAddCard) {
            if(resultCode == Activity.RESULT_OK){
                discount = data.getStringExtra("nameDiscount");
                nameCard = data.getStringExtra("nameCard");
                nameShop = data.getStringExtra("nameShop");
                namePhoto = data.getStringExtra("namePhoto");
                phone = data.getStringExtra("phone");
                picturePass = data.getStringExtra("picturePath");

                listPhoneBook = ContactService.getInstance().getPhoneBooks();
                listPhoneBook.add(0,new PhoneBook(discount, picturePass,namePhoto, nameCard, phone, nameShop));
                sorting();
                adapter = new PhoneBookAdapter(this, ContactService.getInstance().getPhoneBooks());
                lvPhone.setAdapter(adapter);
                saveData();
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Not result", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == requestRefactorCard) {
            if(resultCode == Activity.RESULT_OK){
                String dicoun = data.getStringExtra("discount");
                String name = data.getStringExtra("name");
                String phone = data.getStringExtra("phone");
                String email = data.getStringExtra("email");
                String image = data.getStringExtra("namePhoto");
                String path = data.getStringExtra("imagePath");
                listPhoneBook = ContactService.getInstance().getPhoneBooks();
                listPhoneBook.add(itemPos,new PhoneBook(dicoun,path,image, name, phone, email));
                listPhoneBook.remove(itemPos + 1);
                sorting();
                adapter = new PhoneBookAdapter(this, ContactService.getInstance().getPhoneBooks());
                lvPhone.setAdapter(adapter);
                saveData();
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                adapter = new PhoneBookAdapter(this, ContactService.getInstance().getPhoneBooks());
                lvPhone.setAdapter(adapter);
                Toast.makeText(this, "Not result", Toast.LENGTH_SHORT).show();
            }
        }
    }//onActivityResult


    @Override
   public void onDestroy() {
       super.onDestroy();
       saveData();
   }

    //Create context menu
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, "Delete");
        menu.add(0, CM_REFACTOR_ID, 0, "Refactor");
    }


    public boolean onContextItemSelected(MenuItem item) {
        // Delete choosen element
        switch (item.getItemId()) {
            case CM_DELETE_ID:
                acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                listPhoneBook = ContactService.getInstance().getPhoneBooks();
                listPhoneBook.remove(acmi.position);
                adapter = new PhoneBookAdapter(this, ContactService.getInstance().getPhoneBooks());
                lvPhone.setAdapter(adapter);
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                 saveData();
                break;
            case CM_REFACTOR_ID:

                acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                listPhoneBook = ContactService.getInstance().getPhoneBooks();
                selectedBook = listPhoneBook.get(acmi.position);

                adapter = new PhoneBookAdapter(this, ContactService.getInstance().getPhoneBooks());
                itemPos = (int) adapter.getItemId(acmi.position);

                Intent intent = new Intent (MainActivity.this, RefactorActivity.class);
                intent.putExtra("discount", selectedBook.getmDiscounts());
                intent.putExtra("imagePath", selectedBook.getmPath());
                intent.putExtra("name",selectedBook.getName());
                intent.putExtra("phone", selectedBook.getPhone());
                intent.putExtra("email", selectedBook.getEmail());
                intent.putExtra("image", selectedBook.getsStr());

                startActivityForResult(intent,requestRefactorCard);

                break;
            //
        }
        return super.onContextItemSelected(item);
    }



   //Save data
   public void saveData() {
       ContactService mStudentObject  = ContactService.getInstance();
       SharedPreferences appSharedPrefs = PreferenceManager
               .getDefaultSharedPreferences(this.getApplicationContext());

       SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
       Gson gson = new Gson();
       String json = gson.toJson(mStudentObject);
       prefsEditor.putString("MyObject", json);
       prefsEditor.commit();
   }


   //Loading data
   public void reloadData() {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("MyObject", "");
        ContactService mStudentObject = gson.fromJson(json, ContactService.class);
        ContactService.getInstance().setInstance(mStudentObject);
    }

    public void sorting() {
        Collections.sort(phoneBooks, new Comparator<PhoneBook>() {
            public int compare(PhoneBook one, PhoneBook other) {
                return one.getName().compareTo(other.getName());
            }
        });
    }

}
