package com.example.pavli.qrcodenew;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.bumptech.glide.Glide;

import java.io.File;


public class ActivityCard extends AppCompatActivity {

    ImageView imgQRCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        SeekBar BackLightControl = (SeekBar)findViewById(R.id.backlightcontrol);

        imgQRCard = (ImageView) findViewById(R.id.imgQRCard);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String image = intent.getStringExtra("image");
        String imagePath = intent.getStringExtra("imagePath");

        if (!"".equals(image)) {

            File file = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), image);

            Glide
                    .with(this)
                    .load(file)
                    .into(imgQRCard);
        } else {
            Glide
                    .with(this)
                    .load(imagePath)
                    .into(imgQRCard);
        }

        BackLightControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
                // TODO Auto-generated method stub
                float BackLightValue = (float)arg1/100;

                WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
                layoutParams.screenBrightness = BackLightValue;
                getWindow().setAttributes(layoutParams);
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub
            }});
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
